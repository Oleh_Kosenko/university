class Course {
    constructor(subject, creditPoints) {
       this.subject = subject;
       this.creditPoints = creditPoints;
    }
}

module.exports = { 
    Course
}