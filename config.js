module.exports = {
    MIN_AGE: 17,
    MAX_AGE: 30,
    MAX_CREDIT_POINTS: 6,
    PASSING_SCORE: 20,

    studentNames: ['Yossi', 'Dima', 'Vova'],
    courses: ['Java', 'JavaScript', 'HTML', 'Math', 'History', 'Chemistry', '.Net', 'Python'],
}