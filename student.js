class Student {
    constructor(name, age) {
        this.name = name;
        this.age = age;
        this.grades = [];
    }
}

module.exports = {
    Student
}