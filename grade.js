class Grade {
    constructor(student, course) {
        this.student = student;
        this.course = course;
        this.student.grades.push(this);
    }
}

module.exports = { 
   Grade
}