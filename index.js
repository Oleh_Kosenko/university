const {
    MIN_AGE,
    MAX_AGE,
    MAX_CREDIT_POINTS,
    PASSING_SCORE,

    studentNames,
    courses,
} = require('./config');

const { Student } = require('./student');
const { Course } = require('./course');
const { Grade } = require('./grade');

const randomNumber = max => Math.floor(Math.random() * max);

const students = [];

studentNames.forEach( name => {
    const age = randomNumber(MAX_AGE - MIN_AGE) + MIN_AGE;
    const student = new Student(name, age)
    students.push(student);

    courses.forEach( courseName => {
        const course = new Course(courseName, randomNumber(MAX_CREDIT_POINTS));
        new Grade(student, course);
    });
});

students.forEach( student => {
    console.log(student.name, '--------------------');
    
    
    console.log(student.grades.reduce((prev, current) => 
        `${prev}${current.course.subject}: ${current.course.creditPoints} | `, ''
    ));
});

const passed = students
    .filter(student =>
        student.grades.reduce((prev, current) =>
            prev + current.course.creditPoints
        , 0) > PASSING_SCORE)
    .map(student => student.name);

console.log('passed:', passed);
